import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter as Router,  Route, Routes } from 'react-router-dom';
import Home from './pages';
import About from './pages/about';
import Services from './pages/services';
import Contact from './pages/contact';

import EarthAnimation from './components/EarthAnimation';
import SignUpText from './components/SignUpText';
import InsightAppl from './components/InsightAppl';
// import TextPlaceHolder from "./components/TextPlaceHolder";
import OurPartners from './components/OurPartners';
import ForDevelopers from './components/ForDevelopers';
import HowWeWork from './components/HowWeWork';
import AboutUs from './components/AboutUs';
import OurTeam from './components/OurTeam';

import LocationCarousel from "./components/LocationCarousel";
import Footer from "./components/Footer";
import PrivacyPolicy from "./components/PrivacyPolicy";


//import SignUp from './pages/signup';


function App() {
  
  return (
   
   
    <div>

      <Router>
          <Navbar />
          <Routes>
            <Route path='/' exact component={Home} />
            <Route path='/services' component={Services} />
            <Route path='/about' component={About} />
            <Route path='/contact' component={Contact} />
            
          </Routes>
      </Router>
    
      <EarthAnimation/>
      {/* <div className="seperatorDiv"></div> */}
      <SignUpText/>
      <OurPartners/>
      <InsightAppl/>
      <ForDevelopers/>
      {/* <HowWeWork/> */}

       <LocationCarousel  /> 
      <AboutUs/>
      <OurTeam/>
      <Footer/>
      <PrivacyPolicy/>

    
      {/* <TextPlaceHolder/> */}


    </div>
  );
}

export default App;
