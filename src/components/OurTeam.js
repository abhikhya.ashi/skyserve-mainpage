import React from 'react';

import './OurTeam.css';
import Adithya1 from '../images/Adithya 1.png';
import Adithya2 from '../images/Adithya 2.png';
import Adithya3 from '../images/Adithya 3.png';



function OurTeam() {
  return (

    <>
        
        <div className="teamTitle">
                Meet our Team
        </div>
        


        <div className="teamContainer" >
       
            <div className="teamImgWrapper">
            
                <img className="teamImg" src={Adithya1} alt="Adithya 1" />
                <div className="teamName"> Adithya</div>
                <div className="teamText">Chief of Product. In charge of putting company's experiences into the juicer and serving products for enterprises \& end-users. Blogger-poet by night, and nature photographer on vacations.
                </div>
            </div>
            <div className="teamImgWrapper">
            
                <img className="teamImg" src={Adithya2} alt="Adithya 2" />
                <div className="teamName"> Animesh</div>
                <div className="teamText">Software Engineer Web Technologies. Code Ninja. Full stack. Reacts to a Python in Java. Can build painkiller solutions from scratch. 
                </div>
            </div>
            <div className="teamImgWrapper">
            
                <img className="teamImg" src={Adithya3} alt="Adithya 3" />
                <div className="teamName"> Nayan</div>
                <div className="teamText">Senior software engineer. CodeFather. Nayan is leading the development of software that puts intelligence into satellite payload systems. Loves traveling, curious about new culture and food traditions.
                </div>
            </div>
            

        </div>

    </>

  )
}

export default OurTeam;