import React from 'react';
// import { Link } from 'react-router-dom';
import './ForDevelopers.css';
import devImg1 from '../images/dev-img-1.png';
import devImg2 from '../images/dev-img-2.png';
import devImg3 from '../images/dev-img-3.png';
import devImg4 from '../images/dev-img-4.png';
import devImg5 from '../images/dev-img-5.png';
import devImg6 from '../images/dev-img-6.png';



function ForDevelopers() {
  return (

    <div className="forDevelopersContainer">
        
        <div className="leftDevelopersContainer">        
            <div className="developersTitle">
                For Developers
            </div>
            <div className="developersText">
            Developers can bring their pre-trained models into SkyServe for deployment to all satellites and sensors hosting SkyServe
            </div>
        </div>
        
        <div className="rightDevelopersContainer"> 

            <div className="topImgContainer">
                <img className="devImg1" src={devImg1} alt="dev img 1" />
                <img className="devImg2" src={devImg2} alt="dev img 2" />
                <img className="devImg3" src={devImg3} alt="dev img 3" />
            </div>
            <div className="bottomImgContainer">
                <img className="devImg4" src={devImg4} alt="dev img 4" />
                <img className="devImg5" src={devImg5} alt="dev img 5" />
                <img className="devImg6" src={devImg6} alt="dev img 6" />
            </div>
        
        </div>
      </div>

   
    
  )
}

export default ForDevelopers;