import styled from "styled-components";

export const SlideImage = styled.img`
  // width: 400px;
  height: 400px;
  object-fit: cover;

  width: 720px;
  // height: auto;
  /* margin:5px;    */
  /* height: 433px; */
  border-radius: 10px;
  margin-top: -70px;
  /* z-index: 5; */
`;

export const StyledSlider = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`;