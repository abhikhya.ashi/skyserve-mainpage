import React from 'react'
import './MainTitle.css';


function MainTitle() {
  return (

    <div className="TitleContainer">
        {/* <h3 className="skyserve">S K Y S E R V E</h3> */}
        <div className="Title">Insights as <br/>a Service</div>
      </div>
    
  )
}

export default MainTitle;
