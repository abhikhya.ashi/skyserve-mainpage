import React from 'react';
import './PrivacyPolicy.css';

function PrivacyPolicy() {
  return (
    <div className="privacyPolicyContainer">
        <div className="privacyPolicy">
        Read our Privacy Policy Terms of use
        </div>
        <div className="skyserve2022">
            ©2022, SkyServe Inc.
        </div>
    </div>
  )
}

export default PrivacyPolicy;