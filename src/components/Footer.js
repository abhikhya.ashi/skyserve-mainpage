
import React from 'react';
import {Link} from 'react-router-dom';
import './Footer.css';
import LInLogo from '../images/LinkedIn-logo.png';
import TwitterLogo from '../images/twitter-logo.png';




function Footer() {
  return (

   
        <>

        <div className="footerContainer" >
       
            <div className="footerTitle">
            Insights from <br/>Space for Earth
            </div>
            <div className="footerNavWrapper">
        
                <div className="navFirstColumn"> 
                    <ul className="firstUL">
                        <li>
                            <a href="/aboutus">About Us </a>
                            {/* <Link> About Us</Link> */}
                        </li>
                        <li>
                            <a href="/contactus">Contact Us </a>
                            {/* <Link> Contact Us</Link> */}
                        </li>
                        <li>
                            <a href="/contactus">News</a>
                            {/* <Link> News</Link> */}
                        </li>
                    </ul>
                  
                </div>
                <div className="navSecondColumn"> 
                    <ul className="secondUL">
                        <li>
                            <a href="/contactus">Blog</a>
                            {/* <Link> Blog</Link> */}
                        </li>
                        <li>
                            <a href="/contactus">Career</a>
                            {/* <Link> Career</Link> */}
                        </li>
                        <li>
                            <a href="/contactus">Customers</a>
                            {/* <Link> Customers</Link> */}
                        </li>
                    </ul>
                
                </div>
            </div>
            <div className="socialMediaLinks">
            
                <div className="linWrapper">
                    <img className="linkedInImg" src={LInLogo} alt="Linked In" />
                    <span className="mediaText">LinkedIn</span>
                </div>
                <div className="twitterWrapper">
                    <img className="twitterImg" src={TwitterLogo} alt="Twitter" />
                    <span className="mediaText">Twitter</span>
                </div>
                
            </div>
            

        </div>
        <div className="tiltedDiv">

        </div>

    </>

  )
}

export default Footer;