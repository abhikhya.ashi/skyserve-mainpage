import React, {useState} from 'react';
import LogoImg from '../../images/logo-png-1.png';

import {
  Nav,
  NavLink,
  Bars,
  XBars,
  NavMenu,
  NavBtn,
  NavBtnLink,
  NavbarExtendedContainer,
  NavLinkExtended,
  OpenLinksButton,
  NavbarContainer,
  NavbarInnerContainer,
  LeftContainer,
  RightContainer,
  Logo

} from './NavbarElements';

const Navbar = () => {

   const [extendNavbar, setExtendNavbar] = useState(false);

  return (
    <>
      <NavbarContainer extendNavbar={extendNavbar}>
      <NavbarInnerContainer>
        <LeftContainer>
          <NavLink to='/'>
            {/* <img  src={Logo} alt='logo' /> */}
            <Logo src={LogoImg} alt='logo' />
          </NavLink>
        </LeftContainer>

        <RightContainer>
        <NavMenu>
          <NavLink to='/services' activeStyle>Services</NavLink>
          <NavLink to='/about' activeStyle>About us</NavLink>
          <NavLink to='/contact-us' activeStyle>Contact us</NavLink>
        </NavMenu>

        <NavBtn>
          <NavBtnLink to='/signin'>Sign Up</NavBtnLink>
        </NavBtn>

        <OpenLinksButton
              onClick={() => {
                setExtendNavbar((curr) => !curr);
              }}
        >
          {/* {console.log('before tertiary stmnt, the extendNavvar = ', extendNavbar);} */}
              {extendNavbar ? <XBars/> : <Bars />}
        </OpenLinksButton>
           {/* {console.log('before extendNavbar, the extendNavvar =  ${extendNavbar}`); */}

          {extendNavbar && (
              <NavbarExtendedContainer>
                <NavLinkExtended to='/services' activeStyle>Services</NavLinkExtended>
                <NavLinkExtended to='/about' activeStyle>About us</NavLinkExtended>
                <NavLinkExtended to='/contact-us' activeStyle>Contact us</NavLinkExtended>
              </NavbarExtendedContainer>
           )
          }
          </RightContainer>




      </NavbarInnerContainer>
      </NavbarContainer>
    </>
  );
};

export default Navbar;