import { FaTimes, FaBars } from 'react-icons/fa';
import { NavLink as Link } from 'react-router-dom';
import styled from 'styled-components';


export const Logo = styled.img`
  margin: 10px;
  max-width: 180px;
  height: auto;
  // border: 1px solid red;
`;

// export const Nav = styled.nav`
//   background: #000;
//   height: ${(props) => (props.extendNavbar ? "20vh" : "80px")};
//   display: flex;
//   justify-content: space-between;
//   padding: 0.5rem calc((100vw - 1000px) / 2);
 
//   }
// `;

export const NavbarContainer = styled.nav`
  // width: 100%;
  height: ${(props) => (props.extendNavbar ? "100px" : "80px")};
  background-color: black;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 0.5rem calc((100vw - 1000px) / 2);

  @media (min-width: 700px) {
    height: 80px;
  }
  // position: fixed;
  position: sticky;
  top: 0px;
  bottom-margin: 50px;
  // z-index: 500;
  // opacity: 0.6;
  // border: 1px solid green;
`;

export const NavLink = styled(Link)`
  color: #fff;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;
  &.active {
    color: #15cdfc;
  }
`;

export const Bars = styled(FaBars)`
  display: none;
  color: #fff;
  @media screen and (max-width: 700px) {
    display: block;
    position: absolute;
    // position: relative;
    // margin-top: 0px;
    // margin-right: 0px;
    top: -15px;
    right: -15px;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
    // border: 1px solid red;
  }
`;

export const XBars = styled(FaTimes)`
  display: none;
  color: #fff;
  color: white;
  @media screen and (max-width: 700px) {
    display: block;
    z-index: 260;
    color:grey;
    background-color: white;
    position: absolute;
    top: -15px;
    right: -15px;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
    // border: 1px solid red;
  }
`;


export const NavMenu = styled.div`
  display: flex;
  align-items: center;
  // margin-right: -24px;
  // border: 1px solid red;
  /* Second Nav */
  /* margin-right: 24px; */
  /* Third Nav */
  /* width: 100vw;
  white-space: nowrap; */
  @media screen and (max-width: 700px) {
    display: none;
  }
`;

export const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  margin-left: 20px ;
  // margin-right: 24px;
  // border: 2px solid yellow;
  /* Third Nav */
//  justify-content: flex-end;
  // width: 100vw; 
  @media screen and (max-width: 700px) {
    display: none;
  }
`;


export const NavBtnLink = styled(Link)`
  border-radius: 4px;
//   background: #256ce1;
  background: yellow;
  padding: 10px 22px;
//   color: #fff;
  color: black;
  outline: none;
  border: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;
  /* Second Nav */
  margin-left: 24px;
  // border: 1px solid red;
  &:hover {
    transition: all 0.2s ease-in-out;
    background: #fff;
    color: #010606;
  }
`;

export const NavbarExtendedContainer = styled.div`
  
  position: absolute;
  top: 10px;
  right: 10px;
  z-index: 255;
  display: flex;
  flex-direction: column;
  align-items: center;
  // justify-content: center;
  margin-top: 20px;
  background: white;
  height: auto;
  width: 100%
  // border: 2px solid red;

  @media (min-width: 700px) {
    display: none;
  }


`;
export const NavLinkExtended = styled(Link)`
  background-color: white;
  color: black;
  // margin-top: 32px;
  // top: 50px;
  text-decoration: none;
  padding: 0 1rem;
  width: 100%;
  height: 25%;
  cursor: pointer;
 
  // border: 1px solid red;
  &.active {
    color: #15cdfc;
  }

  font-size: x-large;
  // font-family: Arial, Helvetica, sans-serif;
  text-decoration: none;
  margin: 10px;
`;

export const OpenLinksButton = styled.button`
  // width: 70px;
  // height: 50px;
  // position: absolute;
  // top: 2px;
  // right: 2px;
  width: 5px;
  height: 5px;
  background: none;
  border: none;
  color: black;
  font-size: 45px;
  cursor: pointer;
  // border: 2px solid green;
  @media (min-width: 700px) {
    display:none;
    
  }
`;

export const NavbarInnerContainer = styled.div`
  width: 100%;
  height: 80px;
  display: flex;
  // border: 1px solid red;
`;

export const LeftContainer = styled.div`
  flex: 30%;
  display: flex;
  left: 2px;
  // justify-content: flex-start;
  // padding-left: 50px;
  // border: 1px solid red;
`;

export const RightContainer = styled.div`
  flex: 70%;
  display: flex;
  align-items: center;
  white-space: nowrap;
  // padding-right: 5%;
  // border: 5px solid blue;
  position: relative;
`;