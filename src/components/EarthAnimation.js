import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Earth from '../images/Earth0230.png';
import Satellite from '../images/large-satellite.png';
import Satellite2 from '../images/Satellite2.png';
import "./EarthAnimate.css"
import Circle1 from '../images/Ellipse 9.png';
import Circle3 from '../images/Ellipse 7.png';
import MainTitle from './MainTitle';

function EarthAnimation() {
  return (
    <Container className="containerWrapper">
      
      <Row>
        {/* <Col className="EarthAnimate">1 of 3</Col> */}

        <Col className="EarthAnimate">
          <MainTitle/>
          
          <div className="circle" >
            <div className="LargeCircleContainer">
              <img className="LargeCircle" src={Circle1} alt="large circle" width="670" height="686"/> 
            
           </div>
          
           {/* <img src={Circle3} alt="small circle" />  */}
           <div className="earth-container">
              <img className="earth" src={Earth} alt="the earth"  width="341" height="353" />
           </div>
           <div className="satContainer">
              <img className="satellite" src={Satellite} alt="the satellite" width="25" height="25"/> 
           </div>

           <div className="sat2Container">
              <img className="satellite2" src={Satellite2} alt="2nd satellite" width="25" height="25"/> 
           </div>


           <div className="SmallCircleContainer">
              <img className="SmallCircle" src={Circle3} alt="small circle" width="455" height="455"/> 
            
           </div>
        </div>
        
        {/* <div className="circle">Circle</div>  */}
        </Col>

        {/* <Col className="EarthAnimate">3 of 3</Col> */}
      </Row>
      
    </Container>
  );
}

export default EarthAnimation;