import React from 'react';

import './InsightAppl.css';
import Agriculture from '../images/agriculture.png';
import AssetMonitoring from '../images/asset-monitoring.png';
import ChangeDetection from '../images/change-detection.png';


function InsightAppl() {
  return (

    <>
         <div  className="insightTitle"> Insight Application </div>
        <div className="insightText">
                    SkyServe provides satellite insights to users for better decision-making in different sectors
        </div>
           


    <div className="insightApplContainer" >
       
        <div className="insightImageContainer">
            <div className="singleImgContainer">
                <img className="insightImg" src={ChangeDetection} alt="Change Detection" />
                <div className="imgTitle">Change Detection</div>
                <div className="imgText">Identifying land cover changes, disaster evaluation, pipeline monitoring, and urban expansion study</div>
            </div>
            
            <div className="singleImgContainer">
                <img className="insightImg" src={AssetMonitoring} alt="Assset Monitoring" />
                <div className="imgTitle">Asset Monitoring</div>
                <div className="imgText">With real-time satellite data, you can find and inspect the land, track construction progress, locate equipment, risks</div>
            </div>
            
            <div className="singleImgContainer">
                <img className="insightImg" src={Agriculture} alt="Agriculture" />
                <div className="imgTitle">Agriculture</div>
                <div className="imgText">Enhancing, estimation and decision-making for agriculture produce.</div>
            </div>
        </div>

    </div>
    </>

  )
}

export default InsightAppl;