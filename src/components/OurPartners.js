import React from 'react';
// import { Link } from 'react-router-dom';
import './OurPartners.css';
import nvidia from '../images/nvidia.png';
import SatSure from '../images/SatSure Logo.png';
import esri from '../images/esriPartner.png';



function OurPartners() {
  return (

    <>
    <div className="ourPartnersContainer">
        
        <div className="leftPartnerContainer">        
            <div className="partnerTitle">
                Our Partners
            </div>
            <div className="partnerText">
                Pleasure to work With
            </div>
        </div>
        
        <div className="rightPartnerContainer"> 

            <div className="partnerImgContainer">
                <img className="partnerImg" src={SatSure} alt="Partner SatSure Logo" />
                <img className="partnerImgESRI" src={esri} alt="Partner ESRI Logo" />
                <img className="partnerImg" src={nvidia} alt="Partner NVIDIA Logo" />
            </div>
        
        </div>
      </div>

      <div className="partnerTextBottom">SkyServe provide multiple ways for our <br/>customers to flexibly leverage our platform.</div>
      </>
    
  )
}

export default OurPartners;