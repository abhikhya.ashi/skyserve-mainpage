import React from 'react';
// import { Router, Link } from 'react-router-dom';
import './SignUpText.css';
import signUpArrow from '../images/signup-arrow.png';


function SignUpText() {
  return (

    <div className="signUpTextContainer">
        
        <p className="signUpText">
            Signup here for planetary scale real <br/>time insights for your algorithms
        </p>
        
        <a href="/signin" class="signUpButton">sign up 
        <span> <img src={signUpArrow} alt="sign up img" /></span> 
        </a> 
        {/* <Router>
          <Link to="/signin" class="signUpButton">sign up</Link>
        </Router> */}
         
   
    </div>
    
  )
}

export default SignUpText;