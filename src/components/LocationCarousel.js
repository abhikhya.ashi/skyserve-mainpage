import React from 'react';

import './LocationCarousel.css';
import oneImg from '../images/oneImg.png';
import leftArrow from '../images/leftArrow.png';
import rightArrow from '../images/rightArrow.png';
import locationMap from '../images/locationMap.png';
import Loc2 from '../images/exampleLoc2.jpg';
import Loc3 from '../images/exampleLoc3.jpg';
import Loc4 from '../images/exampleLoc4.jpg';

// import SlideImage from './SlideImage';
import Slider from './Slider';

const ImageData = [
    {
      image: locationMap ,
    },
    {
      image: Loc2 ,
    },
    {
      image: Loc3,
    },
    {
      image: Loc4,
    }
];
  

function LocationCarousel( ) {
  return (

    <>
       
        <div className="howWeWorkTitle">
                     How we Work?
        </div>
        


        <div className="howWeWorkContainer" >
       
            <div className="leftHowWeWorkContainer">
                <div className="leftTopImgContainer" > 
                    <div className="oneImgWrapper">
                        <img className="oneImg" src={oneImg} alt="one image" />
                    </div>
                    <div className="oneImgText">
                            Pick a location<br/> of your interest
                    </div>
                </div>
                <div className="leftBottomImgContainer" > 
                    
                        {/* <img className="leftArrow" src={leftArrow} alt="left Arrow" />
                        <img className="rightArrow" src={rightArrow} alt="right Arrow" /> */}
                </div>
            </div>
            
            <div className="rightHowWeWorkContainer">
               
                    <Slider slides={ImageData} />
               
            </div>
            
            

        </div>
    </>

  )
}

export default LocationCarousel;