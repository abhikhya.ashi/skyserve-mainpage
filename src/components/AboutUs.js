import React from 'react';
// import { Link } from 'react-router-dom';
import './AboutUs.css';
import mainLogo from '../images/Main Logo_Dark Mode.png';



function AboutUs() {

  return (
      
        <>       
            <div className="aboutUsTitle">
                About Us
            </div>

            <div className="aboutUsContainer">
              <div className="aboutUsTextContainer">
                 <div className="aboutUsText">We are an Insights as a Service platform enabling satellite-based edge computed insights for core industries and solution providers to scale faster and more affordably. We feed multispectral imagery for your models deployed on edge and facilitate timely predictions. SkyServe is expanding our offering across satellite constellations and sensing systems to get global coverage and richer, real time insights. 
                 </div>
              </div>
              <div className="logoContainer"> 
                    <img className="logoImg" src={mainLogo} alt="logo Image" />     
              </div>
            </div>
                       
      </> 
    
  )
}

export default AboutUs;