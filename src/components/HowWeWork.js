import React from 'react';

import './HowWeWork.css';
import oneImg from '../images/oneImg.png';
import leftArrow from '../images/leftArrow.png';
import rightArrow from '../images/rightArrow.png';
import locationMap from '../images/locationMap.png';


function HowWeWork() {
  return (

    <>
       
        <div className="howWeWorkTitle">
                     How we Work?
        </div>
        


        <div className="howWeWorkContainer" >
       
            <div className="leftHowWeWorkContainer">
                <div className="leftTopImgContainer" > 
                    <div className="oneImgWrapper">
                        <img className="oneImg" src={oneImg} alt="one image" />
                    </div>
                    <div className="oneImgText">
                            Pick a location<br/> of your interest
                    </div>
                </div>
                <div className="leftBottomImgContainer" > 
                    
                        <img className="leftArrow" src={leftArrow} alt="left Arrow" />
                        <img className="rightArrow" src={rightArrow} alt="right Arrow" />
                </div>
            </div>
            
            <div className="rightHowWeWorkContainer">
                <img className="locationMap" src={locationMap} alt="location map" />    
            </div>
            

        </div>
    </>

  )
}

export default HowWeWork;