import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route, Routes } from 'react-router-dom';
import Home from './pages';
import About from './pages/about';
import Services from './pages/services';
import Contact from './pages/contact';
import SignUp from './pages/signup';
//import background-img from "./images/homepage-bkgrnd.png";

//const background-img = new URL("./images/homepage-bkgrnd.png", import.meta.url);

function App() {
   // url(${background-img})`
  return (
   
    // <div className="main-container"> 
    // <div> 
    <div className="main-container">
     {/* <video  autoplay muted loop
      style = { {
        position: "absolute",
        width: "100%",
        left: "50%",
        top: "50%",
        height: "100%",
        objectFit: "cover",
        transform: "translate(-50%, -50%)",
        zIndex: "-1"
      }} >
      <source src="./video/skyserve-video.mp4" type="video/mp4"/> 
      <source src="https://www.appsloveworld.com/wp-content/uploads/2018/10/Sample-Mp4-Videos.mp4" type="video/mp4"/>
    </video> */}
    
    <Router>
      <Navbar />
      <Routes>
        <Route path='/' exact component={Home} />
      
        <Route path='/services' component={Services} />
        <Route path='/about' component={About} />
        <Route path='/contact' component={Contact} />
        {/* <Route path='/signup' component={SignUp} /> */}
      </Routes>
    </Router>
    {/* <div className="overLay" style={{background: `url("./images/skyserver-edited-img.png")`}}/>  */}
    
    </div>
  );
}

export default App;